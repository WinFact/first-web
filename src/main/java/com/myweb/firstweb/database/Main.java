package com.myweb.firstweb.database;

import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException, InterruptedException {

        PoolConnector poolConnector = PoolConnector.getInstance();

        ConnectorDB connectorDB = poolConnector.getConnection();

        System.out.println(connectorDB.isClosed());

    }

}
