package com.myweb.firstweb.database;

import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class PoolConnector {
    private static PoolConnector instance = null;
    private static ReentrantLock lock = new ReentrantLock();
    private static ArrayBlockingQueue<ConnectorDB> connectionQueue;
    private static final int POOL_SIZE = getPoolSize();

    private PoolConnector() {

    }

    private static int getPoolSize() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("database");
        return Integer.parseInt(resourceBundle.getString("db.pool_size"));
    }

    public static PoolConnector getInstance() {
        lock.lock();

        try {
            if(instance == null) {
                instance = new PoolConnector();
                connectionQueue = new ArrayBlockingQueue<ConnectorDB>(POOL_SIZE);
                for (int i = 0; i < POOL_SIZE; i++) {
                    connectionQueue.offer(new ConnectorDB());
                }
            }
        }finally {
            lock.unlock();
        }
        return instance;
    }

    public ConnectorDB getConnection() throws InterruptedException {
        return connectionQueue.take();
    }

    public void closeConnection(ConnectorDB connection) {
        connectionQueue.offer(connection);
    }

    public void closePool() throws InterruptedException, SQLException {
        lock.lock();

        try {
            for (int i = 0; i < connectionQueue.size(); i++) {
                connectionQueue.take().close();
            }

            connectionQueue = null;
            instance = null;
            
        }finally {
            lock.unlock();
        }
    }

}
